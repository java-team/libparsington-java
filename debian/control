Source: libparsington-java
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Ghislain Antony Vaillant <ghisvail@gmail.com>
Section: java
Priority: optional
Build-Depends: debhelper-compat (= 13),
               default-jdk-headless,
               maven-debian-helper
Build-Depends-Indep: default-jdk-doc,
                     junit4,
                     libmaven-javadoc-plugin-java
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/java-team/libparsington-java
Vcs-Git: https://salsa.debian.org/java-team/libparsington-java.git
Homepage: https://github.com/scijava/parsington

Package: libparsington-java
Architecture: all
Depends: ${misc:Depends},
         ${maven:Depends}
Suggests: ${maven:OptionalDepends},
          libparsington-java-doc <!nodoc>
Description: mathematical expression parser for Java
 Parsington is an infix-to-postfix (or infix-to-syntax-tree) expression
 parser for mathematical expressions written in Java. It is simple yet
 fancy, handling (customizable) operators, functions, variables and
 constants in a similar way to what the Java language itself supports.

Package: libparsington-java-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Recommends: ${maven:DocDepends},
            ${maven:DocOptionalDepends}
Suggests: libparsington-java
Description: documentation for parsington
 Parsington is an infix-to-postfix (or infix-to-syntax-tree) expression
 parser for mathematical expressions written in Java. It is simple yet
 fancy, handling (customizable) operators, functions, variables and
 constants in a similar way to what the Java language itself supports.
 .
 This package contains the API documentation.
Build-Profiles: <!nodoc>
